require 'rubocop'

class String
  def valid?(str)
    brackets_str = str.gsub(/[^\[\]\(\)\{\}\<\>]/, '').gsub('()', '').gsub('[]', '').gsub('{}', '').gsub('<>', '')
    return true if brackets_str.empty?
    return false if brackets_str.length.odd?
    return false if brackets_str.include?(str)
    valid?(brackets_str)
  end
end

p valid?('[ ]') # => true
p valid?('[ ') # => false
p valid?('< >') # => true
p valid?('[ ( ) {} ]') # => true
p valid?('[ ( { ) } ]') # => false
p valid?('[ ( { { ( ) }} )]') # => true
